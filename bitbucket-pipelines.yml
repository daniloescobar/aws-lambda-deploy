image:
  name: python:3.7


setup: &setup
  step:
    name: Setup testing resources
    script:
      # create s3
      - S3_BUCKET="bbci-pipes-test-infra-us-east-1-aws-lambda-deploy-${BITBUCKET_BUILD_NUMBER}"
      - REPOSITORY="bbci-pipes-test-ecr-lambda-${BITBUCKET_BUILD_NUMBER}"
      - STACK_NAME_S3_ECR="bbci-pipes-test-infra-lambda-s3-ecr-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-cloudformation-deploy:0.12.0
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: "us-east-1"
          STACK_NAME: ${STACK_NAME_S3_ECR}
          TEMPLATE: "./test/CloudformationStackTemplate_s3_ecr.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
            [{
              "ParameterKey": "BucketName",
              "ParameterValue": ${S3_BUCKET}
            },
            {
              "ParameterKey": "ECRUser",
              "ParameterValue": "${ECR_LAMBDA_USER}"
            },
            {
              "ParameterKey": "REPOSITORY",
              "ParameterValue": "${REPOSITORY}"
            }]
      # create lambda from s3
      - STACK_NAME="bbci-pipes-test-infrastructure-lambda-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-sam-deploy:1.3.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: 'us-east-1'
          S3_BUCKET: ${S3_BUCKET}
          STACK_NAME: ${STACK_NAME}
          SAM_TEMPLATE: './test/sam_template.yaml'
          CAPABILITIES: [ 'CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND' ]
          WAIT: 'true'
      # create lambda in ECR container
      - wget https://github.com/aws/aws-sam-cli/releases/download/v1.18.0/aws-sam-cli-linux-x86_64.zip
      - echo '5b9c61df9a92f0e9f7d335a124ae5d94cc10a11b424fa2635e0cfaeb56bdd3e3 aws-sam-cli-linux-x86_64.zip' | sha256sum -c -
      - unzip aws-sam-cli-linux-x86_64.zip -d /opt/sam-installation && /opt/sam-installation/install
      - apt-get update && apt-get install --no-install-recommends -y jq gettext-base
      - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.2.9.zip" -o "awscliv2.zip" && unzip awscliv2.zip
      - echo 'c778f4cc55877833679fdd4ae9c94c07d0ac3794d0193da3f18cb14713af615f awscliv2.zip' | sha256sum -c - && ./aws/install

      - export ACCOUNT=$(aws sts get-caller-identity | jq -r .Account)
      - cd test/sam-ecr-app && sam build
      # later, when deploying and packaging from image uri is supported in aws-sam-deploy, use it for deploying such function from ecr image
      - sam package --image-repository ${ACCOUNT}.dkr.ecr.us-east-1.amazonaws.com/${REPOSITORY} --template-file .aws-sam/build/template.yaml
      - STACK_NAME_LAMBDA_ECR="bbci-pipes-test-infra-lambda-ecr-${BITBUCKET_BUILD_NUMBER}"
      - sam deploy --stack-name ${STACK_NAME_LAMBDA_ECR} --image-repository ${ACCOUNT}.dkr.ecr.us-east-1.amazonaws.com/${REPOSITORY} --capabilities CAPABILITY_IAM
      - cd -
    services:
      - docker


release-dev: &release-dev
  step:
    name: Release development version
    trigger: manual
    image: python:3.7
    script:
      - set -ex
      - pip install semversioner
      - VERSION=$(semversioner current-version).${BITBUCKET_BUILD_NUMBER}-dev
      - pipe: atlassian/bitbucket-pipe-release:4.2.0
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
          GIT_PUSH: 'false'
          VERSION: ${VERSION}
    services:
      - docker


test: &test
  parallel:
    - step:
        oidc: true
        name: Test
        services:
        - docker
        script:
          - pip install -r test/requirements.txt
          - flake8 --extend-ignore E501,E125
          - pytest -p no:cacheprovider test/test_unit.py --verbose --capture=no
          - pytest test/test.py --verbose --capture=no
        after-script:
          - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.2.9.zip" -o "awscliv2.zip" && unzip awscliv2.zip
          - echo 'c778f4cc55877833679fdd4ae9c94c07d0ac3794d0193da3f18cb14713af615f awscliv2.zip' | sha256sum -c - && ./aws/install
          # empty ecr repository and s3 bucket
          - S3_BUCKET="bbci-pipes-test-infra-us-east-1-aws-lambda-deploy-${BITBUCKET_BUILD_NUMBER}"
          - REPOSITORY="bbci-pipes-test-ecr-lambda-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" ecr delete-repository --repository-name ${REPOSITORY} --force
          - aws --region="us-east-1" s3 rb s3://${S3_BUCKET} --force
          # delete ecr-s3 stack
          - STACK_NAME_S3_ECR="bbci-pipes-test-infra-lambda-s3-ecr-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_S3_ECR}
          # delete lambda stacks
          - STACK_NAME="bbci-pipes-test-infrastructure-lambda-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME}
          - STACK_NAME_LAMBDA_ECR="bbci-pipes-test-infra-lambda-ecr-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_LAMBDA_ECR}
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


push: &push
  step:
    name: Push and Tag
    script:
      - pipe: atlassian/bitbucket-pipe-release:4.2.0
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
    services:
      - docker


pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push
    qa-*:
    - <<: *test
    - <<: *push
  custom:
    test-and-report:
      - step:
          caches:
            - maven
          image: maven:3.3.9
          services:
            - docker
          script:
            - export DOCKERHUB_IMAGE='bitbucketpipelines/${BITBUCKET_REPO_SLUG}'
            - export DOCKERHUB_TAG=${BITBUCKET_BUILD_NUMBER}
            # Install bats, AWS CLI and shellcheck
            - apt-get update && apt-get install -y bats shellcheck python3-pip
            # Shell check
            - shellcheck pipe.sh
            # Install AWS CLI
            - apt-get update && apt-get install -y
            - pip3 install awscli --upgrade --user
            - export PATH=$PATH:/root/.local/bin/
            # Build the pipe
            - docker build -t ${DOCKERHUB_IMAGE}:${BITBUCKET_BUILD_NUMBER} -t ${DOCKERHUB_IMAGE}:latest .
            # Run tests
            - set +e
            - bats test/test*; test_exit_code=$?
            - set -e
            - echo test_exit_code = $test_exit_code
            - pipe: atlassian/report-task-test-result:1.0.0
              variables:
                DATADOG_API_KEY: $DATADOG_API_KEY
                TASK_NAME: aws-lambda
                TEST_EXIT_CODE: $test_exit_code
            - exit $test_exit_code
